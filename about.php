<!-- Get Header -->
<?php include("phpextras/header.php"); ?>
      <div id="mainHeader">
        <div class="wrap">
          <img src="images/tasty_logo.png" alt="Tasty Recipes Logo" />
        </div>
        <!-- end mainHeader wrap -->
      </div>
      <!-- end mainHeader -->
    </div>
    <!-- end header -->
    <div id="recipeContent">
      <div class="wrap">
        <div id="recipeText">
          <img src="images/pancakes_calendar.png" alt="Tasty Recipes Logo" />
          <h1>About Tasty Recipes</h1>
          <p id="intro">
            This text is only here to validate the page layout. It isn't worth reading. This line was intentionally left blank. You are currently reading text that is written in English, not any other language. Be careful not to waste too much time reading placeholder text! Determining whether the typeface works or not is only possible if there is text for it to be applied to.
          </p>
          <p>
            Determining whether the typeface works or not is only possible if there is text for it to be applied to. This is just dummy text that is essentially a placeholder so you can see what your final typefaces will look like. The text that you are reading is only to fill the space visually.
          </p>
          <p>
            Placeholder text is useful when you need to see what a page design looks like, but the actual content isn't available. It's like having someone with identical measurements check the fit of a dress before trying it on yourself. Placeholder text is useful when you need to see what a page design looks like, but the actual content isn't available. It's like having someone with identical measurements check the fit of a dress before trying it on yourself.
          </p>
          <p>
            There needs to be something here, even though it's not what you might expect on a finished website. Often when a web designer needs to fill in a paragraph temporarily, they will use some nonsensical Latin words; Not in this paragraph though. If you are reviewing this page, it is possible that it will be up to you to provide the content that will replace these sentences.
          </p>
        </div>
      </div>
      <!-- end conent wrap -->
    </div>
    <!-- end content -->
  </body>
</html>

<!-- Get Header -->
<?php include("phpextras/header.php"); ?>
      <div id="mainHeader">
        <div class="wrap">
          <img src="images/tasty_logo.png" alt="Tasty Recipes Logo" />
        </div>
        <!-- end mainHeader wrap -->
      </div>
      <!-- end mainHeader -->
    </div>
    <!-- end header -->
    <div id="calendarContent">
      <?php date_default_timezone_set('CET'); ?>
      <div class="wrap">
          <div id="calendar">
            <div class="month">
              <h1><?php  echo date('F Y'); ?></h1>
            </div>
            <div class="align-center">
              <ul class="weekdays">
                <li>Mo</li>
                <li>Tu</li>
                <li>We</li>
                <li>Th</li>
                <li>Fr</li>
                <li>Sa</li>
                <li>Su</li>
              </ul>
              <br class="clear" />
              <!-- generate calendar -->
              <ul class="days">
                <?php
                  $inputMonth = date('y-m-j');
                  $month = date("m" , strtotime($inputMonth));
                  $year = date("Y" , strtotime($inputMonth));
                  $datenr = date('t');
                  $getdate = getdate(mktime(null, null, null, $month, 1, $year));
                  $getdate2 = getdate(mktime(null, null, null, $month, $datenr, $year));
                  $day = $getdate["wday"];
                  $lastday = $getdate2["wday"];

                  $temp = 0;
                  if ($day == 0)
                  {
                    $day = 7;
                  };
                  for ($k = 1; $k < $day; $k++)
                  {
                    echo '<li></li>';
                    $temp++;
                  };
                  for ($i = 1; $i <= date('t'); $i++)
                  {
                    if ($i == 12)
                    {
                      echo '<li id="meatballs" class="recipeday"><a href="./meatballs.php">' . $i . '</a></li>';
                    }
                    elseif ($i == 20)
                    {
                      echo '<li id="pancakes" class="recipeday"><a href="./pancakes.php"><p>' . $i . '</p></a></li>';
                    }
                    elseif ($i == date('d'))
                    {
                      echo '<li id="current_date"><p>' . $i . '</p></li>';
                    }
                    else
                    {
                      echo '<li><p>' . $i . '</p></li>';
                    };
                  };
                  for ($i=$lastday; $i < 7; $i++)
                  {
                    echo '<li></li>';
                  };
                ?>
              </ul>
              <br class="clear" />
          </div>
        </div>
        <!-- end calendarbox -->
      </div>
      <!-- end calendarContent wrap -->
    </div>
    <!-- end calendarContent -->

    <div id="footer">
      <div class="wrap">
        &nbsp;
      </div>
      <!-- end footer wrap -->
    </div>
    <!-- end footer -->
  </body>
</html>

function openLogin() {
    document.getElementById("login").style.visibility = "visible";
    document.getElementById("login").style.opacity = "1";
    document.getElementById("login").style.display = 'block';
}

function closeLogin() {
  document.getElementById("login").style.opacity = "0";
  document.getElementById("login").style.visibility = "hidden";
  document.getElementById("login").style.display = 'none';
}

function displayRegForm() {
  document.getElementById("loginForm").style.opacity = 0;
  document.getElementById("loginForm").style.visibility = "hidden";
  document.getElementById("loginForm").style.display = "none";
  document.getElementById("regForm").style.opacity = 1;
  document.getElementById("regForm").style.visibility = "visible";
  document.getElementById("regForm").style.display = "block";
  document.getElementById("register").style.visibility = "hidden";
}

function displayLoggedIn() {
  document.getElementById("loginForm").style.display = "none";
  document.getElementById("regForm").style.display = "none";
  document.getElementById("register").style.display = "none";
  document.getElementById("loginbutton").value = "Log out";
  document.getElementById("loginbutton").setAttribute("onclick", "Javascript:window.location.href = '/tasty/phpextras/logout.php'");
}


function atload() {
  openLogin();
  if (bool)
  {
    displayLoggedIn();
  };
}

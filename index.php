<!-- Get Header -->
<?php include("phpextras/header.php"); ?>
      <div id="mainHeader">
        <div class="wrap">
          <img src="images/tasty_logo.png" alt="Tasty Recipes Logo" />
          <br class="clear" />
          <a href="calendar.php" class="button">Recipe Calendar</a>
        </div>
        <!-- end mainHeader wrap -->
      </div>
      <!-- end mainHeader -->
    </div>
    <!-- end header -->

    <div id="recipes">
      <div class="wrap">
        <ul>
          <li class="container">
              <img src="images/pancakes.png" alt="Pancakes" />
              <div class="overlay">
                <div class="text">
                  <h3>Pancakes</h3>
                  <a href="./pancakes.php">Recipe</a>
                </div>
              </div>
          </li>
          <li class="container">
              <img src="images/meatballs.png" alt="Swedish meatballs" />
              <div class="overlay">
                <div class="text">
                  <h3>Meatballs</h3>
                  <a href="./meatballs.php">Recipe</a>
                </div>
              </div>
          </li>
          <li>
            <a href="#">
              <img src="images/placeholder.png" alt="Brows more recipes" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="images/placeholder.png" alt="Placeholder" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="images/placeholder.png" alt="Placeholder" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="images/placeholder.png" alt="Placeholder" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="images/placeholder.png" alt="Placeholder" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="images/placeholder.png" alt="Placeholder" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="images/placeholder.png" alt="Placeholder" />
            </a>
          </li>
        </ul>
      </div>
      <!-- end recipes wrap -->
    </div>
    <!-- end recipes -->

  </body>
</html>

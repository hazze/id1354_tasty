<!-- Get Header -->
<?php include("phpextras/header.php"); ?>
      <div id="mainHeader">
        <div class="wrap">
          <img src="images/tasty_logo.png" alt="Tasty Recipes Logo" />
        </div>
        <!-- end mainHeader wrap -->
      </div>
      <!-- end mainHeader -->
    </div>
    <!-- end header -->
    <div id="recipeContent">
      <div class="wrap">
        <div id="recipeText">
          <?php include("phpextras/xmlprint.php");?>
          <h2 id="commenttitle">Comments</h2>
          <div id="comments">
            <?php include("phpextras/print_comments.php"); ?>
          </div>
          <?php include("phpextras/comment_form.php"); ?>
        </div>
      </div>
      <!-- end conent wrap -->
    </div>
    <!-- end content -->
  </body>
</html>

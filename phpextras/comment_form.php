<?php if ($_SESSION["login"]) { ?>
  <div id="commentForm">
    <form action="phpextras/comment.php" method="POST" id="comment">
      <input
        id="title"
        class="inputfield"
        type="text"
        name="title"
        placeholder="Subject"
        autocomplete="off"
        required
      />

      <textarea
        id="text"
        class="inputfield"
        type="text"
        name="text"
        placeholder="Comment text"
        autocomplete="off"
        required></textarea>

      <input
        type="submit"
        id="submit"
        class="button"
        value="Comment"
        name="submit"
      />
    </form>
  <?php
  }
  else
  {
    echo "<p>You need to login to write comments</p>",
         "<a onclick='openLogin()' class='button'>Log in</a>";
  }; ?>

<?php
  session_start();
  $_SESSION["currenturl"] = $_SERVER[REQUEST_URI];
  include("initdb.php");
?>
<!DOCTYPE html>
<html xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css?version=<?php time();?>" />

    <title>Tasty Recipes | Home</title>

    <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>

    <script src="../tasty/functions.js"></script>
    <?php if ($_SESSION["formcomplete"]) { ?>
      <script>
        var bool = '<?php echo $_SESSION["login"]; ?>';
        window.onload = atload;
      </script>
    <?php $_SESSION["formcomplete"] = false; };
      if ($_SESSION["login"])
      { ?>
        <script>
          window.onload = displayLoggedIn;
        </script>
      <?php };?>
  </head>
  <body>
    <div id="login" class="loginoverlay">
      <div id="overlaycontent">
        <p id="register" onclick="displayRegForm()" class="button">Sign up</p>
        <p onclick="closeLogin()">&times;</p>
        <br class="clear" />
        <?php if($_SESSION["login"])
          echo "<img src='images/avatar_green.png' alt='Silhouette of a person' />";
          else
            echo "<img src='images/avatar.png' alt='Silhouette of a person' />";
          ?>
        <form action="phpextras/login.php" method="POST" id="loginForm">
          <input
            class="user inputfield"
            type="text"
            name="username"
            placeholder="Username"
            autocomplete="off"
            pattern="[a-zA-Z0-9]+"
            required
          />
          <input
            class="password inputfield"
            type="password"
            name="password"
            placeholder="Password"
            autocomplete="off"
            pattern="[a-zA-Z0-9]+"
            required
          />
          <input
            type="submit"
            class="submit button"
            value="Log In"
            name="submit"
          />
        </form>

        <form action="phpextras/reguser.php" method="POST" id="regForm">
          <input
            class="user inputfield"
            type="text"
            name="username"
            placeholder="Username"
            autocomplete="off"
            pattern="[a-zA-Z0-9]+"
            required
          />
          <input
            class="password inputfield"
            type="password"
            name="password"
            placeholder="Password"
            autocomplete="off"
            pattern="[a-zA-Z0-9]+"
            required
          />
          <input
            class="confirmpassword inputfield"
            type="password"
            name="confirmpassword"
            placeholder="Confirm Password"
            autocomplete="off"
            pattern="[a-zA-Z0-9]+"
            required
          />
          <input type="submit" class="submit button" value="Register" name="submit" />
        </form>
        <div class="response">
          <?php echo $_SESSION["error"];?>
        </div>
      </div>
    </div>
    <div id="header">
      <div id="nav">
        <div class="wrap">
          <a href="./">
            <img src="images/tasty_logo_small.png" alt="Tasty Recipes Logo Small" id="navLogo" />
          </a>
          <div id="login_nav">
            <?php echo "<p id='user'>" . $_SESSION["currentuser"] . "&nbsp;</p>";?>
            <input type="button" onclick="openLogin()" id="loginbutton" class="button" value="Log In" />
          </div>
          <ul>
            <li>
              <a href="./">Home</a>
            </li>
            <li>
              <a href="./calendar.php">Calendar</a>
            </li>
            <li>
              <a href="./#recipes">Recipes</a>
            </li>
            <li>
              <a href="./about.php">About</a>
            </li>
          </ul>
          <br class="clear" />
        </div>
        <!-- end nav wrap -->
      </div>
      <!-- end nav -->

<?php
  session_start();
  $_SESSION["login"] = false;
  $_SESSION["error"] = "";
  $_SESSION["formcomplete"] = false;
  $_SESSION["errorreason"] = "";
  $_SESSION["currentuser"] = "";

  header("Location: ".$_SESSION["currenturl"]);
  die();
?>

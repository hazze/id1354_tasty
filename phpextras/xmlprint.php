<?php
  session_start();
  $page = $_SESSION["currenturl"];
  $find = '/tasty/';
  $page = str_replace($find, "", $page);

  if ($page == "pancakes.php")
    $xmlfile = file_get_contents("./xml/pancakes.xml");
  else
    $xmlfile = file_get_contents("./xml/meatballs.xml");



  $cookbook = new SimpleXMLElement($xmlfile);
  echo "<img src='".$cookbook->recipe->imagepath."' alt='Picture of ".$cookbook->recipe->title."' />";
  echo "<h1>".$cookbook->recipe->title,
      "<span>".$cookbook->recipe->quantity." servings</span></h1>";
  echo "<ul id='info'>";
  echo "<li>Prep: ".$cookbook->recipe->preptime."</li>",
       "<li>Cook: ".$cookbook->recipe->cooktime."</li>",
       "<li>Ready in: ".$cookbook->recipe->totaltime."</li></ul>";

  echo "<p id='intro'>",
       $cookbook->recipe->{'description'}->li,
       "</p>";
  echo "<br class='clear' />",
       "<div class='layoutlists'>";
  echo "<h2>Ingredients</h2><ul>";

  foreach ($cookbook->recipe->ingredient->li as $li)
    echo "<li>".$li."</li>", PHP_EOL;

  echo "</div><div class='layoutlists'>",
       "<h2>Instructions</h2><ol>";
  foreach ($cookbook->recipe->recipetext->li as $li)
    echo "<li>".$li."</li>", PHP_EOL;

  echo "</div>";
